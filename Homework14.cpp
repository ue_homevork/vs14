﻿
#include <iostream>
#include <string>


int main()
{
    std::string s = "Hello World!";
    std::cout << s << "\n";

    //вывод количества символов
    std::cout << s.length() << " letters\n";

    //Первый способ
    //указала функцию front для доступа к первому символу
    //вывожу значение функции front
    char& f1 = s.front();
    f1 = 'H';
    std::cout << f1 << '\n';
    //указала функцию back для доступа к последнему символу
    //вывожу значение функции back
    char& back1 = s.back();
    back1 = 'd';
    std::cout << back1 << '\n';

    //Второй способ
    //обращение к индексам. не зависит от символов и длины текста
    std::cout << s[0] <<"\n" << s[s.length() - 1] << "\n";

    return 0;


}
